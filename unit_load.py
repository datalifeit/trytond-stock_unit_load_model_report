# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateReport


class UnitLoadPrintMixin(object):

    def _get_unit_load(self):
        Unitload = Pool().get('stock.unit_load')
        return Unitload(Transaction().context.get('active_id'))

    def _get_unit_loads(self):
        Unitload = Pool().get('stock.unit_load')
        return [Unitload(id_) for id_ in
            Transaction().context.get('active_ids')]

    def _do_print(self, action, action_report):
        Action = Pool().get('ir.action')
        data = {
            'ids': Transaction().context['active_ids'],
            'id': Transaction().context['active_id'],
        }
        if action_report:
            action = action_report.action
            action = Action.get_action_values(action.type, [action.id])[0]
        return action, data


class WizardMixin(object):

    start = StateTransition()

    def check_end(self, end='end'):
        if self.uls2action:
            return 'print_'
        return end

    def transition_start(self):
        pool = Pool()
        IrModel = pool.get('ir.model')

        uls = self._get_unit_loads()
        self.uls2action = {}
        for ul in uls:
            Transaction().set_context(active_id=ul.id)
            action_report = IrModel.get_report('stock.unit_load', self.report,
                self.report_pattern(ul),
                iteration_filter=self._get_pattern_iteration_filter())
            self.uls2action.setdefault(action_report, [])
            self.uls2action[action_report].append(ul)

        return self.check_end()

    def report_pattern(self, ul):
        return ul.get_report_pattern()

    def do_print_(self, action):
        action_report = list(self.uls2action.keys())[0]
        uls = self.uls2action.pop(action_report)
        Transaction().set_context(active_ids=[ul.id for ul in uls])
        return self._do_print(action, action_report)

    def transition_print_(self):
        return self.check_end()

    @classmethod
    def _get_pattern_iteration_filter(cls):
        return ('origin', )


class UnitLoadPrintLabel(Wizard, WizardMixin, UnitLoadPrintMixin):
    '''Print unit load label'''
    __name__ = 'stock.unit_load.label.print'

    report = 'stock.unit_load.label'

    print_ = StateReport(report)


class UnitLoadPrintCaseLabel(Wizard, WizardMixin, UnitLoadPrintMixin):
    """Wizard unit load print case label"""
    __name__ = 'stock.unit_load.case_label.print'

    report = 'stock.unit_load.case_label'

    print_ = StateReport(report)


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    def get_report_pattern(self, party=False):
        return {}
