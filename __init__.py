# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import unit_load


def register():
    Pool.register(
        unit_load.UnitLoad,
        module='stock_unit_load_model_report', type_='model')
    Pool.register(
        unit_load.UnitLoadPrintLabel,
        unit_load.UnitLoadPrintCaseLabel,
        module='stock_unit_load_model_report', type_='wizard')
